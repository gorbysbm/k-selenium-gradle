package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SignInPage extends BasePage{

    //*********Constructor*********
    public SignInPage(WebDriver driver) {
        super(driver);
    }

    //*********Web Elements*********
    private By username = By.cssSelector("input[name='username']");
    private By loginButton = By.cssSelector("button[type='submit']");
    private By errorMessageText = By.cssSelector(".alert.alert-danger");

    //*********Page Methods*********


    public void enterUsername(String username){
        clearAndTypeText(this.username,username);
    }

    public void clickLogin(){
        WebElement element = driver.findElement(loginButton);
        scrollIntoView(driver, element);
        click(loginButton);
    }

    //TODO: no current error message from blank password?
    public void verifyPasswordErrorText(String expectedText) {
        assertTextEquals(errorMessageText, expectedText);
    }
}