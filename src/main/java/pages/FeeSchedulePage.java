package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class FeeSchedulePage extends BasePage{

    public FeeSchedulePage(WebDriver driver) {
        super(driver);
    }

    //*********Page Variables*********
    private String feeSchedulesUrl = "https://www.kraken.com/en-us/features/fee-schedule";

    //Go to FeesPage
    public void goToFeesPage() {
        driver.get(feeSchedulesUrl);
    }

    //*********Web Elements*********
    private By feesTable =By.cssSelector(".kraken-table .nowrap tbody");

    //*********Page Methods*********

    /*
    Input the row number and the cell number to get the data.
    e.g. enter 1,1 to get the first cell in the first row
    */
    public String returnCellFromFeesTable(int rowPosition, int cellPosition){
        return readCellInTable(feesTable, --rowPosition, --cellPosition);
    }
}