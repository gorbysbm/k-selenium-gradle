package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CreateAccountPage extends BasePage{

    public CreateAccountPage(WebDriver driver) {
        super(driver);
    }

    //*********Page Variables*********
    private String createAccountUrl = "https://www.kraken.com/sign-up";

    //Go to Create Account Page
    public void goToCreateAccountPage() {
        driver.get(createAccountUrl);
    }

    //*********Web Elements*********
    private By advancedOptions =By.cssSelector("#adv-opt-trigger");
    private By selectCountry =By.cssSelector("select[name='country']");

    //*********Page Methods*********
    public void clickAdvancedOptions(){
        WebElement element = driver.findElement(advancedOptions);
        scrollIntoView(driver,element);
        element.click();
    }

    public void selectCountry(String country){
        selectFromDropdown(selectCountry, country);
    }
}