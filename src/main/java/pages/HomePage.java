package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class HomePage extends BasePage {

    //*********Constructor*********
    public HomePage(WebDriver driver) {
        super(driver);
    }

    //*********Page Variables*********
    private String baseURL = "http://www.kraken.com/";

    //*********Web Elements*********
    private By loginLink = By.cssSelector("a[href='/login']");


    //*********Page Methods*********
    //Go to Homepage
    public void goToHomepage() {
        driver.get(baseURL);
    }

    public void clickSignIn(){
        click(loginLink);
    }

}