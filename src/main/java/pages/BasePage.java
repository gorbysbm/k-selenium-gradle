package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.List;

public class BasePage {
    final WebDriver driver;
    private final WebDriverWait wait;

    BasePage (WebDriver driver){
        this.driver = driver;
        wait = new WebDriverWait(driver,10);
    }

    //Wait for presence of element before proceeding with action
    protected void waitForPresence(By elementBy) {
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(elementBy));
    }

    protected void click (By elementBy) {
        waitForPresence(elementBy);
        driver.findElement(elementBy).click();
    }

    protected void selectFromDropdown(By elementBy, String itemName){
        Select dropdown = new Select(driver.findElement(elementBy));
        dropdown.selectByValue(itemName);
    }

    protected void clearAndTypeText(By elementBy, String text) {
        waitForPresence(elementBy);
        driver.findElement(elementBy).clear();
        driver.findElement(elementBy).sendKeys(text);
    }

    protected String readText (By elementBy) {
        waitForPresence(elementBy);
        return driver.findElement(elementBy).getText();
    }

    protected String readCellInTable (By elementBy, int rowIndex,int cellIndex) {
        waitForPresence(elementBy);
        WebElement baseTable = driver.findElement(elementBy);
        List<WebElement> tableRows = baseTable.findElements(By.tagName("tr"));

        //basic boundary case check
        if (rowIndex>=tableRows.size())
            Assert.fail( "Invalid position of table row, it should be greater than 0 and <="+ tableRows.size());
        List<WebElement> tableCells = tableRows.get(rowIndex).findElements(By.tagName("td"));
        if (cellIndex>=tableCells.size())
            Assert.fail( "Invalid position of cell, it should be greater than 0 and <="+ tableCells.size());

        return tableCells.get(cellIndex).getText();
    }

    protected void assertTextEquals(By elementBy, String expectedText) {
        waitForPresence(elementBy);
        Assert.assertEquals(readText(elementBy), expectedText);
    }

    protected static void scrollIntoView(WebDriver driver, WebElement element){
        ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();", element);
    }
}
