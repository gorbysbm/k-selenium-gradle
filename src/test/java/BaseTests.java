import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class BaseTests {

    WebDriver driver;

    @Parameters({ "isDocker" })
    @BeforeClass(groups= {"default"})
    public void setup (boolean isDocker) throws MalformedURLException {
        if (isDocker){
            DesiredCapabilities capabilities = DesiredCapabilities.chrome();
            driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"),capabilities);
        }
        else{
            driver = new ChromeDriver();
        }
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();

    }

    @AfterClass(groups= {"default"})
    public void tearDown() {
        if(null != driver) {
            driver.close();
            driver.quit();
        }
    }

}
