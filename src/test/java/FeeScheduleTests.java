import org.testng.annotations.Test;
import pages.FeeSchedulePage;

public class FeeScheduleTests extends BaseTests {

    @Test (groups= {"k-interview" }, description = "Navigates to the Fees page and grabs the cell data from the table")
    public void readDataFromTableTest() {
        FeeSchedulePage feeSchedulePage = new FeeSchedulePage(driver);

        feeSchedulePage.goToFeesPage();
        //Example fetch data from a table
        String dataFromTable = feeSchedulePage.returnCellFromFeesTable(9, 1);
        System.out.println("The data from the table was: "+ dataFromTable);
    }
}
