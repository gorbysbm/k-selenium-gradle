import org.testng.annotations.Test;
import pages.CreateAccountPage;

public class CreateAccountTests extends BaseTests {

    @Test (groups= {"k-interview" }, description = "Navigates to the Create Account page and selects a Country from dropdown")
    public void createAccount() {
        CreateAccountPage createAccountPage = new CreateAccountPage(driver);

        createAccountPage.goToCreateAccountPage();
        createAccountPage.clickAdvancedOptions();
        createAccountPage.selectCountry("AD");
    }

}
