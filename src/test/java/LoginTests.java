import org.testng.annotations.Test;

import pages.HomePage;
import pages.SignInPage;

public class LoginTests extends BaseTests {

    @Test (groups= {"k-interview" }, description = "Navigates to Sign in page, enters a standard  username, " +
            "but leaves the password blank. Expected: a warning message that password can't be blank." +
            "Uncomment this code to see the failure: //signInPage.verifyPasswordErrorText")
    public void invalidLoginTest() {
        HomePage homePage = new HomePage(driver);
        SignInPage signInPage = new SignInPage(driver);

        homePage.goToHomepage();

        //Example of clicking page link
        homePage.clickSignIn();

        //Example of text entry
        signInPage.enterUsername("ourUsername");

        //Example of button click
        signInPage.clickLogin();

        //TODO: Uncomment me to see a failing test
        //This will Fail as there is currently no error message returned for a blank password
        //signInPage.verifyPasswordErrorText("Password cannot be left blank");

    }

}
