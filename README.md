## Prerequisites

* install ```gradle``` if not present

```
        https://gradle.org
```

* navigate to project root directory and add ```gradlew``` 

```
        gradle wrapper
```


## Installing and Running on Mac OSX instructions


* install ```brew``` if not present:

```
        /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

* install ```chromedriver``` 

```
        brew tap homebrew/cask && brew cask install chromedriver
```

### Running the Tests: 

* navigate to the project root directory and run: 
``` 
        ./gradlew clean test
```
NOTE: You can view a failing test scenario by uncommenting the line at: ```LoginTests.invalidLoginTest.signInPage.verifyPasswordErrorText```

=========================================================


## Installing and Running with Docker Instructions


* install ```docker``` if not present:

```
        https://www.docker.com/
```

* Pull / setup the ```selenium-standalone-chrome``` container  by searching for it here:

```
        https://hub.docker.com/u/selenium/
```

### Running the tests with Docker: 

* start selenium-chrome-debug container specifying current chrome version in the command (e.g. 3.141.59-lithium): 
``` 
        docker run -d -p 4444:4444 -p 5900:5900 -v /dev/shm:/dev/shm -e VNC_NO_PASSWORD=1 selenium/standalone-chrome-debug:3.141.59-lithium
```

* navigate to the project root directory and run: 
``` 
        ./gradlew clean testdocker
```
NOTE: This is a ```vnc``` enabled container. You can connect to  ```localhost:5900``` to see the test execution
